# Nota de Aula XI
#
#
# DIY
# Do it Yourself!
#
#
#11.1

print("\n####################################################################################################\n")
languages = ["Ingles", "Alemão", "Espanhol", "Japonês", "Francês"]
print("Os três primeiros itens da lista são: ")
print(languages[:3])

print("\nOs três últimos itens da lista são: ")
print(languages[2:])

print("\nOs três itens no meio da lista são: ")
print(languages[1:4])
print("\n####################################################################################################\n")
#
#
#11.2
pizza_hot = ["Franco", "Banana", "Portuguesa"]
pizza_hot_paraguai = pizza_hot[:]
print(pizza_hot)
print(pizza_hot_paraguai)
pizza_hot.append("Moda")
pizza_hot_paraguai.append("Sorvete")
print(pizza_hot)
print(pizza_hot_paraguai)
print("\n####################################################################################################\n")
#
#
#11.3
languages = ["Ingles", "Alemão", "Espanhol", "Japonês", "Francês"]
for frist_languages in languages[:3]:
    print(frist_languages)

for last_languages in languages[2:]:
    print(last_languages)

for middle_languages in languages[1:4]:
    print(middle_languages)

print("\n####################################################################################################\n")