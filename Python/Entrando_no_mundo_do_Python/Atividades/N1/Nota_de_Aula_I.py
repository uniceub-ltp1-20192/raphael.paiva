# Nota de Aula I
#
#
# DIY
# Do it Yourself!
#
#
# 1.1

print ("\nJá é meu segundo programa\n\n\n")
#
# 1.2

print ("1- Cadastrar\n")
print ("2- Logar\n")
print ("3- Sair\n\n\n")
#
#
# 1.3

print("Os números pares de 1 até 10 são:")
for pair in range(2, 11, 2):
    print(pair, end="\n")