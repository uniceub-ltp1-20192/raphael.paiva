# Nota de Aula VIII
#
#
# DIY
# Do it Yourself!
#
#
#8.1

print("\n####################################################################################################\n")

#Pense em pelo menos cinco lugares no mundo que você gostaria de visitar.
#Armazene os locais em uma lista. Certifique­se de que a lista não esteja em ordem alfabética.
lugares = ['Orlando', 'Alemanha', 'Munique', 'Acre', 'New York']

#Imprima sua lista na ordem original. Não se preocupe em imprimir a lista item por item, basta imprimi­la como uma lista bruta do Python.
print("\n____________________________________________________________\n")
print("Lista Original:\n",lugares)

#Use sorted() para imprimir sua lista em ordem alfabética sem modificar a lista atual.
print("\n____________________________________________________________\n")
print("\nLista com sorted:\n",sorted(lugares))

#Mostre que sua lista ainda está em sua ordem original, imprimindo­a.
print("\n____________________________________________________________\n")
print("\nLista Original:\n",lugares)

#Use sorted() para imprimir sua lista em ordem alfabética inversa sem alterar a ordem da lista original.
print("\n____________________________________________________________\n")
print("\nLista alfabética inversa:\n",sorted(lugares, reverse=True))
print("\nLista Original:\n",lugares)

#Use reverse() para alterar a ordem da sua lista. Imprima a lista para mostrar que a ordem foi alterada.
print("\n____________________________________________________________\n")
print("\nLista Original:\n",lugares)
lugares.reverse()
print("\nLista Original com 'reverse':\n",lugares)

#Use reverse() para alterar a ordem da sua lista novamente. Imprima a lista para mostrar que ela está de volta ao pedido original.
print("\n____________________________________________________________\n")
lugares.reverse()
print("\nLista Original com 'reverse' pela segunda vez:\n",lugares)

#Use sort() para que a lista seja armazenada em ordem alfabética. Imprima a lista para mostrar que a ordem foi alterada.
print("\n____________________________________________________________\n")
lugares.sort()
print("\nLista com 'sort':\n",lugares)

#Use sort() para que a lista seja armazenada em ordem alfabética inversa. Imprima a lista para mostrar que a ordem foi alterada.
print("\n____________________________________________________________\n")
lugares.sort(reverse=True)
print("\nLista com 'sort' ordem alfabética inversa:\n",lugares)
print("\n####################################################################################################\n")
#
#
#8.2 Altere os códigos desenvolvidos para o convite de jantar (Notas de aula 7) para que seja apresentada a quantidade de convidados existentes.

convidados =['Dwayne Johnson','Jason Momoa','Chris Hemsworth','Robert Downey Jr.']
print("Olá, " + convidados[0] + ", encontrei uma mesa de jantar maior, então temos mais lugares disponíveis, mudaremos de mesa.")
print("Olá, " + convidados[1] + ", encontrei uma mesa de jantar maior, então temos mais lugares disponíveis, mudaremos de mesa.")
print("Olá, " + convidados[2] + ", encontrei uma mesa de jantar maior, então temos mais lugares disponíveis, mudaremos de mesa.")
print("Olá, " + convidados[3] + ", encontrei uma mesa de jantar maior, então temos mais lugares disponíveis, mudaremos de mesa.")
print("\n")
convidados.insert (0, "Gary Oldman")
convidados.insert (2, "J. K. Rowling")
convidados.append ("Emma Watson")
print("\n")
print(convidados)
convidados.append("Keanu Reeves")
print(convidados)
print("Convidados Existentes: ")
print(len(convidados))
print("\n####################################################################################################\n")
#
#
#8.3 Todas as funções!
letras = ['X', 'Z', 'Y', 'A', 'B', 'C']
print(letras)
print("\nUsando somente sort")
letras.sort()
print(letras)


print("\nUsando sort com reverse=True:")
print(letras)
letras.sort(reverse=True)
print(letras)

print("\nUsando sorted:")
print(sorted(letras))

print("\nUsando sorted com o parâmetro reverse=True:")
print(sorted(letras, reverse=True))

print("\nUsando reverse: ")
letras.reverse()
print(letras)

print("\nUsando reverse novamente para a lista voltar ao que era antes do primeiro reverse: ")
letras.reverse()
print(letras)

print("\nUsando len: ")
letras = ['X', 'Z', 'Y', 'A', 'B', 'C']
print(len(letras))
print("\n####################################################################################################\n")