# Nota de Aula VII
#
#
# DIY
# Do it Yourself!
#
#
#7.1
print("\n####################################################################################################\n")

names = ['Dwayne Johnson','Jason Momoa','Chris Hemsworth','Robert Downey Jr.']

for i in range (4):
  print("Olá, ",names[i],"você esta convidado para jantar comigo!")
print("\n####################################################################################################\n")
#
#
#7.2

names = ['Dwayne Johnson','Jason Momoa','Chris Hemsworth','Robert Downey Jr.']

for i in range (4):
  print("Olá, ",names[i],"você esta convidado para jantar comigo!")
print(names[0],", não poderá comparecer!")


names.remove('Dwayne Johnson')
names.append('Alan Rickman')

for i in range (4):
  print("Olá, ",names[i],"você esta convidado para jantar comigo!")
print("\n####################################################################################################\n")
#
#
#7.3

names = ['Dwayne Johnson','Jason Momoa','Chris Hemsworth','Robert Downey Jr.']

for i in range (4):
  print("Olá, ",names[i],"você esta convidado para jantar comigo!")
print(names[0],", não poderá comparecer!")


names.remove('Dwayne Johnson')
names.append('Alan Rickman')

for i in range (4):
  print("Olá, ",names[i],"você esta convidado para jantar comigo!")

print("Encontrei uma mesa maior, e resolvi fazer um grande jantar, chamei mais 3 amigos!")

names.insert(0,'Gary Oldman')
names.insert (2,'Bolsonaro')
names.append('Caio')

for i in range (6):
  print("Olá, ",names[i],"Vamos fazer um grande jantar! E você está convidado!")
print("\n####################################################################################################\n")
#
#
#7.4

names = ['Dwayne Johnson','Jason Momoa','Chris Hemsworth','Robert Downey Jr.']

for i in range (4):
  print("Olá, ",names[i],"você esta convidado para jantar comigo!")
print(names[0],", não poderá comparecer!")


names.remove('Dwayne Johnson')
names.append('Alan Rickman')

for i in range (4):
  print("Olá, ",names[i],"você esta convidado para jantar comigo!")

print("Encontrei uma mesa maior, e resolvi fazer um grande jantar, chamei mais 3 amigos!")

names.insert(0,'Gary Oldman')
names.insert (2,'Bolsonaro')
names.append('Caio')

for i in range (6):
  print("Olá, ",names[i],"Vamos fazer um grande jantar! E você está convidado!")

print("Ocorreu um problema, e não terá uma grande mesa!")

for x in range(4):
  print(names.pop(x)," Desculpe, você não está mais convidado.")
names.remove('Alan Rickman')
print(" Desculpe, você não está mais convidado.")

for y in range(2):
  print(names[y], "Você deu sorte, ainda está convidado!")

del names[1]
del names[0]

print(names)
print("\n####################################################################################################\n")