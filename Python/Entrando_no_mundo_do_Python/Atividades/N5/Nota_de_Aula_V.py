# Nota de Aula V
#
#
# DIY
# Do it Yourself!
#
#
#5.1
print("\n####################################################################################################\n")

num = int(input("insira um numero: "))
if (num % 2) == 0:
   print("{0} é par".format(num))
else:
   print("{0} é impar".format(num))
print("\n####################################################################################################\n")
#
# 
#5.2

num = int(input("insira um numero: "))  

if num > 1: 
      
   # Iterate from 2 to n / 2  
   for i in range(2, num//2): 
         
       # If num is divisible by any number between  
       # 2 and n / 2, it is not prime  
       if (num % i) == 0: 
           print(num, "Não é um numero primo") 
           break
   else: 
       print(num, "É um numero primo ") 
  
else: 
   print(num, "Não é um numero primo")
print("\n####################################################################################################\n")
#
# 
#5.3

num_one = int(input("Primeiro numero: "))
num_two = int(input("Segundo numero: "))

add = num_one + num_two
sub = num_one - num_two
mul = num_one * num_two
div = num_one / num_two

print("soma = ", add, ", subtração = ", sub, ", Multiplicação = ", mul, ", divisão = ", div)
print("\n####################################################################################################\n")
#
#
#5.4

age = int(input("Qual sua idade: "))
total_sem = (int(input("Quantos semestre tem o curso todo: ")))/2
actual_sem = (int(input("Qual semestre esta cursando: ")))/2
left_sem = (total_sem - actual_sem)
print("você ira se formar em :",left_sem ,"anos")
print("\n####################################################################################################\n")
#
#
#5.5

total_sem = (int(input("Quantos semestre tem o curso todo: ")))/2
actual_sem = (int(input("Qual semestre esta cursando: ")))/2
late_sem = int(input("Quantos semestres está atrasado: "))

print("Você teve ao todo ",late_sem + total_sem, "anos de curso")
print("\n####################################################################################################\n")