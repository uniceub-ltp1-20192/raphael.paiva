# Nota de Aula IV
#
#
# DIY
# Do it Yourself!
#
#
#4.1
print("\n####################################################################################################\n")

name_one = "  \njuninho"
name_two = "\tMarcelo   "
name_three = "  \nCarlos  "
name_four = "\n\tMaria juana"

print(name_one)
print(name_two)
print(name_three)
print(name_four)

print(name_one.lstrip())
print(name_two.rstrip())
print(name_three.strip())
print(name_four)
print("\n####################################################################################################\n")
#
# 
#4.2

name = input("insira um nome:")
name_final = name.replace(" ","")
print(len(name_final))
print("\n####################################################################################################\n")