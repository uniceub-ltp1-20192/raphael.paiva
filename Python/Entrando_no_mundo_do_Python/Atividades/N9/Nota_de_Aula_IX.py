# Nota de Aula IX
#
#
# DIY
# Do it Yourself!
#
#
#9.1

print("\n####################################################################################################\n")

lista = ["7", "8", "13"]
soma = 0
for numero in lista:
    soma += int(numero)
    print(soma)
print("o último valor da lista é o resultado da adição")
print("\n####################################################################################################\n")
#9.2
lista = ["7", "8", "13"]
mult = 1
for numero in lista:
    mult *= int(numero)
    print(mult)
print("o último valor da lista é o resultado da multiplicação")
print("\n####################################################################################################\n")
#9.3
lista = ["7", "8", "13"]
print(min(lista))
print("\n####################################################################################################\n")
#9.4
ListaA = ["ABC", "DEF", "GHI", "YWZ"]
ListaB = ["CEB", "YWZ", "ABC"]
resultado = list(set(ListaA) & set(ListaB))
print(resultado)
print("\n####################################################################################################\n")
#9.5 
ListaA = ["Groenlândia", "Japão", "Alemanha", "Russia", "Vaticano"]
ListaA.sort()
print(ListaA)
print("\n####################################################################################################\n")
#9.6 
ListaA = ["Groenlândia", "Japão", "Alemanha", "Russia", "Vaticano"]
print(ListaA)
for a in ListaA:
    print(len(a))
print("\n####################################################################################################\n")
#
#
#9.7
ListaD = ["Japão", "Alemanha", "Russia", "Vaticano"]

anagrama = 'Votanica'

for ListaC in ListaD:
    if sorted(anagrama) == sorted(ListaC):
        print(ListaC,'é anagrama de', anagrama)

print("\n####################################################################################################\n")