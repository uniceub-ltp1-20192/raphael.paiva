class Armas:
  
  def __init__(self,calibre="",marca="",municao="",peso=0):
    self._calibre = calibre
    self._marca = marca
    self._municao = municao
    self._peso = peso

  @property
  def identificador(self):
    return self._identificador

  @identificador.setter
  def identificador(self,identificador):
    self._identificador = identificador

  @property
  def marca(self):
    return self._marca

  @marca.setter
  def marca(self,marca):
    self._marca = marca 

  @property
  def municao(self):
    return self._municao

  @municao.setter
  def municao(self,municao):
    self._municao = municao

  @property
  def peso(self):
    return self._peso

  @peso.setter
  def peso(self,peso):
    self._peso = peso

  @property
  def calibre(self):
    return self._calibre

  @calibre.setter
  def calibre(self,calibre):
    self._calibre

  def preparar(self):
    print("Pista Quente!!!")

  def atirar (self):
    print("pahh!!!")

