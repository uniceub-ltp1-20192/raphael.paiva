package main

import (
	"fmt"
)

// função principal
func qnota() {

	// solicita nome para o usuário
	qnota := askString("Quantas notas?")
	fmt.Print("A quantidade de notas é:", qnota)

	return
}

func askString(message string) string {
	fmt.Print(message)

	var input string

	fmt.Scanln(&input)

	return input
}
