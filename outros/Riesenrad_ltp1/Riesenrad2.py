import sys

cabinNumber = 1
userCount = 0

def askName():
	return input("Nome: ")

def askHeigth():
	return input("Altura: ")

# solicita nome para o usuário
name    = askName()

# solicita altura para o usuário
heigth  = askHeigth()

print("Olá", name, "sua altura é de", heigth, "m")

sys.exit()

# solicita quantidade de acompanhantes para o usuário
friends = askNumberFriends()

# verifica se o valor armazenado na variável name é uma string
# e armazena o resultado em vs
vs    = validateString(name)

# verifica se o valor armazenado na variável heigth é um float
# e armazena o resultado em vf
vf    = validateFloat(heigth)

# verifica se o valor armazenado na variável friends é um inteiro
# e armazena o resultado em vi
vi    = validateInt(friends)

# checa validade das entradas
if (not(vs and vf and vi)):
	print("Não pode!")
	sys.exit() 

# verifica se as entradas correspondem a realidade
# do cenário apresentado
vname    = validateName(name)
vheigth  = validateHeigth(heigth)
vfriends = validateFriends(friends)

# chaca validade da realidade
if (not(vname and vheigth and vfriends)):
	print("Tá Errraado!")
	sys.exit() 

# verifica se o usuário pode utilizar a roda gigante
allowed = checkPermission(name, heigth, friends)

if not allowed:
	print("Bye bye!")
	sys.exit() 

# verifica se usuário ganhou o prêmio
uhuu = checkPrize()	
