package main

import(
	"fmt"
	"strings"
	"strconv"
)

// função principal
func riesenrad(){
	
	// solicita nome para o usuário
	name:=askString("Seu nome: ")
	
	// verifica se a string contém chars invalidos
	if(!validateString(name)){
		fmt.Println("Tchau! ")
		return
	}
	if(!validateReal(name)){
		fmt.Println("Tchau! ")
        return
	}
	
	// solicita a altura do usuário
	height:=askString("Sua altura: ")

	if(!validateFloat(height)){
		fmt.Println("Tchau! ")
		return
	}
	if(!validateReal(height)){
		fmt.Println("Tchau! ")
        return
	}

	// solicita a quantidade de acompanhantes para o usuário
	friends:=askString("Quantos acompanhantes: ")

	if(!validateInt(friends)){
		fmt.Println("Tchau! ")
		return
	}
	if(!validateReal(friends)){
		fmt.Println("Tchau! ")
		return
	}
	
	
	return
}
// função para gravar o valor das variaveis solicitadas ao
// usuário em cada variavel.
func askString(message string) string{
	fmt.Print(message)
	
	var input string
	
	fmt.Scanln(&input)
	
	return input
}
// Função que verifica se todos os valores 
// são pertencentes ao alfabeto
func validateString(s string) bool{
	return !strings.ContainsAny(s, "1234567890!@#$%().,;:?")
}

func validateFloat(s string) bool{
	_, err := strconv.ParseFloat(s, 64)
	return err == nil
}

func validateInt(s string) bool{
	_, err := strconv.ParseUint(s, 10, 64)
	return err == nil
}

func validateReal(name string,height float64,friends int) bool{
	len(name)>3 && len(name)<=255
	return name
	height>=1.0 and height <2.5
	return height
	friends>=0 and friends<=10
}

func CheckPermission(name string,height float64,friends int) bool{
	name==strings.Count(name, " ") && height>1.61 && friends>1
}


