# Na cidade de canavieiro do leste, chegou o parque de diversões "Palhaço Louco". Uma das atrações é a Roda Giagante.
# Na Roda Gigante só podem entrar pessoas com dois sobrenomes, com mais de 1,61 m de autura e que não estejão sozinhas.
# Porém, se a pessoa tiver exatamente 1,50 de altura e trouçer 4 aconpanhantes, pode entrar.
# Além disto, as cabines são numeradas, e se a pessoa entrar na cabine de número 10 e for a centésima pessoa a fazer isto, ganha o prêmio do Palhaço Louco.
#
# Fornecido ocontexto acima, elabore o algorito na abordagem TopDown, que verifique se a pessoa pode ou não entrar no brinquedo e se poderá ou não ser prêmiada.

# Importando Bibliotecas
import string
import random

# Criando Funções para o funcionamento da Roda-Gigante

# Função valida nome 'vname'. Verifica se a pessoa tem pelo menos 2 sobrenomes! 

def vname():
    name = input('Wie ist dein voller Name?\n')   # Pergunta o nome e registra na variavel name.
    sname = int (sum ([i.strip(string.punctuation).isalpha() for i in name.split()]) -1)   # Separa e conta o numero de palavras/caracteres, semparados por um
                                                                                           # espaço, dentro de uma frase(nome completo), somando uma unidade a
                                                                                           # cada "palavra" e diminuindo uma unidade que é o primeiro nome.
    if sname >= 2:   # Compara se o valor recebido de 'sname'(sobrenome) é maior ou igual a 2 (quantidade nescessária de sobrenomes), se a comparação
        vhohe()      # for verdadeira, segue o programa para 'vhohe'(valida altura).
    if sname < 2:           #Se o valor recebido de 'sname' for menor do que 2, escreve na tela uma resposta.
        print('\nSINTO MUITO! Você precisar ter pelo menos DOIS sobrenomes para entrar na roda gigante.')

# Função valida altura 'vhohe'. Verifica a altura da pessoa.

def vhohe():
    hohe = int(input('\nWas ist deine höhe(in zentimetern)?\n'))   # Pergunta a altura da pessoa em centimetros e registra o valor inteiro na variavel hohe. 
    if hohe >= 161:   # Compara se o valor da variavel hohe for maior ou igual a 161, se a comparação for verdadeira, segue para 'vallein'(valida sozinho).
        vallein()
    if hohe == 150:   # Compara se o valor da variavel hohe for igual a 150, se a comparação for verdadeira, segue para 'vhohe150'(valida altura 150).
        vhohe150()
    if hohe < 161 and hohe !=150:   # Compara se o valor da variavel hohe for menor que 161 e diferente de 150, se a comparação for verdadeira, 
        khohe()                     # segue para 'khohe'(sem altura).

# Função valida sozinho 'vallein'. Verifica se a pessoa está sozinha ou acompanhada.

def vallein():
    allein = input('\nDu bist allein?(j/n)\n')   # Pergunta se a pessoa está sozinha e pede uma resposta no caracter 's' ou 'n', registrada na variavel allein.
    if allein == 'n':   # Compara se o valor da variavel allein for igual a 'n', se a comparação for verdadeira, segue para 'preis'(premio).
        preis()
    if allein == 'j':   # Compara se o valor da variavel allein for igual a 's', se a comparação for verdadeira, escreve na tela uma resposta.
        print('\nVocê precisa de amigos! Não é permitido entrar sozinho na roda gigante.')

# Função Valida altura 150 'vhohe150'. valida a entrada se a pessoa tiver pelo menos 4 acompanhantes.

def vhohe150():
    freunde = int(input('\nWie viele Freunde sind bei dir?\n'))   # Pergunta a quantidade de amigos/acompanhantes e registra o valor inteiro na variavel freunde. 
    if freunde < 4:   # Compara se o valor da variavel freunde for menor que 4, se a comparação for verdadeira, segue para 'khohe'(sem altura).
        khohe()
    if freunde >= 4:   # Compara se o valor da variavel freunde for maior ou igual a 4, se a comparação for verdadeira, segue para 'preis'(premio).
        preis()

# Função Premio 'preis'. Confirma a entrada da pessoa é verifica se ela foi premiada.

def preis():
    print('Pode entrar na roda gigante!')   # Escreve na tela uma confirmação de entrada.
    kabine = random.randint(1,1000)   # Sorteia um numero de 1 à 1000 e registra em 'kabine', referente a cabine número (10) e a centésima pessoa a entrar nela.
    if kabine == 1000:   # Compara se o valor da variavel kabine for igual a 1000, se a comparação for verdadeira, escreve na tela uma resposta.
        print('\n   PARABÉNS! Você ganhou o prêmio.\n   Pode descabelar o palhaço louco!! \n')


# Função sem altura 'khohe'. Escreve uma resposta na tela.

def khohe():   # Escreve uma resposta na tela.
    print('\nSINTO MUITO! Você precisar ter pelo menos 1,61m,\n OU\nter 1,50m e trazer 4 acompanhantes.')



#####-----------------------------------------------------------------------------------------------------------------------------------------#####
#________ Início Do Código______________________________


print ('\nWillkommen im BadeLand! Wolfsburgs bester Vergnügungspark!\nUm das Riesenrad zu fahern, antworte:\n')

vname()


#name = input('Wie ist dein voller Name?\n')

#hohe = int(input('\nWas ist deine höhe(in zentimetern)?\n'))

#allein = input('\nDu bist allein?(Ja/Nein)\n')

#freunde = input('\nWie viele Freunde sind bei dir?\n')




