package main

import "fmt"

func main() {

	var sum_grades, sum_weighted_grades, sum_weighted, grade, weight, average_grade, average_weighted, num_off_grades float64
	var ask_more_grade string

	for {

		fmt.Print("Nota:")
		fmt.Scanln(&grade)
		fmt.Print("Peso(1-5):")
		fmt.Scanln(&weight)

		sum_grades += grade

		sum_weighted_grades += grade * weight

		sum_weighted += weight

		num_off_grades += 1

		fmt.Print("deseja inserir mais uma nota?")
		fmt.Scanln(&ask_more_grade)
		if ask_more_grade == "não" {
			break
		}

	}

	average_grade = sum_grades / num_off_grades
	average_weighted = sum_weighted_grades / sum_weighted

	fmt.Print("\n")
	fmt.Print("-----------------------------------------------")
	fmt.Print("\nMédia: ", average_grade, "\nMédia ponderada: ", average_weighted)

}
